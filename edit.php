<?php

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once($CFG->dirroot.'/local/school_year/lib.php');
require_once($CFG->dirroot.'/local/school_year/edit_form.php');

$id = optional_param('id', 0, PARAM_INT);
$contextid = optional_param('contextid', 0, PARAM_INT);
$context = context_system::instance();
$delete    = optional_param('delete', 0, PARAM_BOOL);
$confirm   = optional_param('confirm', 0, PARAM_BOOL);

require_login();
require_capability('local/schoolyear:manage', $context);

if ($id) {
    $school_year = $DB->get_record('school_year', array('id'=>$id), '*', MUST_EXIST);
}

// Setup the page.
$PAGE->set_context($context);
$PAGE->set_url(new moodle_url('/local/school_year/edit.php',  array('id' => $school_year->id)));
$PAGE->set_pagelayout('standard');
$PAGE->set_heading(get_string('pluginname', 'local_school_year'));

if ($delete and $school_year->id) {
    $PAGE->url->param('delete', 1);
    if ($confirm and confirm_sesskey()) {
        delete_school_year($school_year);
        $_SESSION["delete_successfully"] = "Delete successfully!";
        redirect(new moodle_url('/local/school_year/index.php'));
    }
    $strheading = get_string('deleteschoolyear', 'local_school_year');
    $PAGE->navbar->add(get_string('administrationsite'), new moodle_url('/admin/search.php'));
    $PAGE->navbar->add(get_string('pluginname', 'local_school_year'), new moodle_url('/local/school_year/index.php'));
    $PAGE->navbar->add($strheading);
    $PAGE->set_title($strheading);
    echo $OUTPUT->header();
    echo $OUTPUT->heading($strheading);

    $yes_url = new moodle_url('/local/school_year/edit.php',
        array('delete' => 1,
            'id' => $school_year->id,
            'confirm' => 1,
            'sesskey' => sesskey()));
    $cancel_url = new moodle_url('/local/school_year/index.php');
    $message = get_string('delconfirm', 'local_school_year', format_string($school_year->name));

    echo $OUTPUT->confirm($message, $yes_url, $cancel_url);
    echo $OUTPUT->footer();
    die;
}

// Form
$editform = new school_year_edit_form(null, array('data'=>$school_year));
if ($editform->is_cancelled()) {
    redirect(new moodle_url('/local/school_year/index.php'));
} elseif ($data = $editform->get_data()) {
    session_start();
    if ($data->id) {

        // update school year
        update_school_year($data);
        $_SESSION["update_successfully"] = "Update successfully!";
    } else {

        // add new school year
        add_school_year($data);
        $_SESSION["add_new_successfully"] = "Add new successfully!";
    }
    redirect(new moodle_url('/local/school_year/index.php'));
}

if ($id) {
    $strheading = get_string('editschoolyear', 'local_school_year');
    $breadcrumb = get_string('editschoolyear', 'local_school_year');
    $title = get_string('editschoolyear', 'local_school_year');
} else {
    $strheading = '';
    $breadcrumb = get_string('addnewschoolyear', 'local_school_year');
    $title = get_string('addnewschoolyear', 'local_school_year');
}

// Set the breadcrumb Dashboard > Club.
$PAGE->navbar->ignore_active();
if (has_capability('local/schoolyear:manage', $context)) {
    $PAGE->navbar->add(get_string('administrationsite'), new moodle_url('/admin/search.php'));
    $PAGE->navbar->add(get_string('pluginname', 'local_school_year'), new moodle_url('/local/school_year/index.php'));
    $PAGE->navbar->add($breadcrumb);
}

// Output the header.
$PAGE->set_title($title);
echo $OUTPUT->header();
echo $OUTPUT->heading($strheading);

$baseurl = new moodle_url('/local/school_year/edit.php');
$editcontrols = school_year_edit_controls($context, $baseurl);
if (!$id && $editcontrols) {
    echo $OUTPUT->render($editcontrols);
} else {
    //
}

echo $editform->display();
// Now output the footer.
echo $OUTPUT->footer();

