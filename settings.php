<?php

defined('MOODLE_INTERNAL') || die();

global $CFG;

if ($hassiteconfig) {
    $ADMIN->add('root', new admin_category('school_year',
        get_string('pluginname', 'local_school_year', null, true)));

    $ADMIN->add('school_year', new admin_externalpage('manage_school_year',
        get_string('manageschoolyear', 'local_school_year', null, true), new moodle_url('/local/school_year/index.php'), 'local/schoolyear:manage'));
}
