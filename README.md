### update 16/10/2019
Moodle local plugin - Chức năng quản lý năm học.

## Cài đặt
**Cách 1**
1. Đổi tên thư mục thành `school_year`
2. Nén thành file `.zip` 
3. Tải file zip trong `Moodle -> Sit administrator -> Plugins -> Install Plugins`

**Cách 2**
1. Đổi tên thư mục thành `school_year`
2. Sao chép thư vào trong `Moodle/local`
3. Upgrade lại Moodle 

## Sử dụng
1. Quản lý năm học trong `Site administrator -> School year -> Manage school year`
