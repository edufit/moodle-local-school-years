<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/school_year/lib.php');

$page = optional_param('page', 0, PARAM_INT);

require_login();

$context = context_system::instance();

// Setup the page.
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_url(new moodle_url('/local/school_year/index.php'));
$PAGE->set_title(get_string('pluginname', 'local_school_year'));
$PAGE->set_heading(get_string('pluginname', 'local_school_year'));

// Set the breadcrumb Dashboard > Club.
$PAGE->navbar->ignore_active();
if (has_capability('local/schoolyear:manage', $context)) {
    $PAGE->navbar->add(get_string('administrationsite'), new moodle_url('/admin/search.php'));
    $PAGE->navbar->add(get_string('pluginname', 'local_school_year'), new moodle_url('admin/category.php?category=school_year'));
}

// Output the header.
echo $OUTPUT->header();

show_notifications();

// show menu tabs
$baseurl = new moodle_url('/local/school_year/index.php');
$editcontrols = school_year_edit_controls($context, $baseurl);
if ($editcontrols) {
    echo $OUTPUT->render($editcontrols);
}

$school_years = get_all_school_years(0, 25);

// Output pagination bar.
echo $OUTPUT->paging_bar($school_years['total_school_years'], $page, 25, $baseurl);

$table = generate_table($school_years, $context, $OUTPUT);
echo html_writer::table($table);

// Now output the footer.
echo $OUTPUT->footer();



