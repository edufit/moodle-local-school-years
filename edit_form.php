<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');

class school_year_edit_form extends moodleform {

    /**
     * Define the cohort edit form
     */
    public function definition() {
        global $CFG;

        $mform = $this->_form;
        $school_year = $this->_customdata['data'];

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        $mform->addElement('text', 'name', get_string('name', 'local_school_year'), 'maxlength="254" size="50"');
        $mform->addRule('name', get_string('required'), 'required', null, 'client');
        $mform->setType('name', PARAM_TEXT);

        $mform->addElement('textarea', 'description', get_string('description', 'local_school_year'), 'cols="49"');
        $mform->setType('description', PARAM_RAW);

        $mform->addElement('text', 'academic_year', get_string('academicyear', 'local_school_year'), 'maxlength="254" size="50"');
        $mform->setType('academic_year', PARAM_TEXT);

        $mform->addElement('date_selector', 'quarter_first', get_string('quarter1', 'local_school_year'));
        $date = (new DateTime())->setTimestamp(usergetmidnight(time()));
        $date->modify('+1 day');
        $mform->setDefault('quarter_first', $date->getTimestamp());
        $mform->addRule('quarter_first', get_string('required'), 'required');

        $mform->addElement('date_selector', 'quarter_second', get_string('quarter2', 'local_school_year'));
        $date = (new DateTime())->setTimestamp(usergetmidnight(time()));
        $date->modify('+2 month');
        $mform->setDefault('quarter_second', $date->getTimestamp());
        $mform->addRule('quarter_second', get_string('required'), 'required');

        $mform->addElement('date_selector', 'quarter_third', get_string('quarter3', 'local_school_year'));
        $date = (new DateTime())->setTimestamp(usergetmidnight(time()));
        $date->modify('+4 month');
        $mform->setDefault('quarter_third', $date->getTimestamp());
        $mform->addRule('quarter_third', get_string('required'), 'required');

        $mform->addElement('date_selector', 'quarter_fourth', get_string('quarter4', 'local_school_year'));
        $date = (new DateTime())->setTimestamp(usergetmidnight(time()));
        $date->modify('+6 month');
        $mform->setDefault('quarter_fourth', $date->getTimestamp());
        $mform->addRule('quarter_fourth', get_string('required'), 'required');

        $mform->addElement('date_selector', 'last_day', get_string('lastday', 'local_school_year'));
        $date = (new DateTime())->setTimestamp(usergetmidnight(time()));
        $date->modify('+8 month');
        $mform->setDefault('last_day', $date->getTimestamp());
        $mform->addRule('last_day', get_string('required'), 'required');

        $this->add_action_buttons();

        $this->set_data($school_year);
    }

}

