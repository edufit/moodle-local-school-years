<?php

/**
 * @package   local_school_year_management
 * @copyright 2019 Moodle Pty Ltd (http://moodle.com)
 * @author    dung.duongtrung
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$plugin->version  = 2019101600;
$plugin->requires = 2019052000;
$plugin->component = 'local_school_year';
$plugin->cron = 0;
$plugin->release = '1.0';
$plugin->maturity = MATURITY_STABLE;