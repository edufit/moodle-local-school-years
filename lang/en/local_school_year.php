<?php

/**
 * @package   local_school_year_management
 * @copyright 2019 Moodle Pty Ltd (http://moodle.com)
 * @author    dung.duongtrung
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'School year';
$string['schoolyearlist'] = 'School year list';
$string['addnewschoolyear'] = 'Add new School year';
$string['editschoolyear'] = 'Edit School year';
$string['deleteschoolyear'] = 'Delete School year';
$string['manageschoolyear'] = 'Manage School year';
$string['name'] = 'Name';
$string['description'] = 'Description';
$string['academicyear'] = 'Academic Year';
$string['quarter1'] = 'Quarter 1';
$string['quarter2'] = 'Quarter 2';
$string['quarter3'] = 'Quarter 3';
$string['quarter4'] = 'Quarter 4';
$string['lastday'] = 'Last Day';
$string['days'] = 'Days';
$string['hours'] = 'Hours';
$string['delconfirm'] = 'Do you really want to delete school year \'{$a}\'?';
