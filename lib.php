<?php

function school_year_edit_controls(context $context, moodle_url $currenturl)
{
    $tabs = array();
    $currenttab = 'view';

    if (has_capability('local/schoolyear:manage', $context)) {
        $viewurl = new moodle_url('/local/school_year/index.php');
        $tabs[] = new tabobject('viewcschoolyear', new moodle_url($viewurl), get_string('schoolyearlist', 'local_school_year'));
        if ($currenturl->get_path() === $viewurl->get_path()) {
            $currenttab = 'viewcschoolyear';
        }

        $addurl = new moodle_url('/local/school_year/edit.php');
        $tabs[] = new tabobject('addcschoolyear', $addurl, get_string('addnewschoolyear', 'local_school_year'));
        if ($currenturl->get_path() === $addurl->get_path() && !$currenturl->param('id')) {
            $currenttab = 'addcschoolyear';
        }

    }
    if (count($tabs) > 1) {
        return new tabtree($tabs, $currenttab);
    }
    return null;
}

function get_all_school_years($page = 0, $per_page = 25)
{
    global $DB;

    $fields = "SELECT sc.*";
    $count_fields = "SELECT COUNT(*)";
    $sql = " FROM {school_year} sc ";
    $params = array();
    $where_sql = '';

    $total_school_years = $allcohorts = $DB->count_records_sql($count_fields . $sql . $where_sql, $params);

    $order = " ORDER BY sc.name ASC, sc.academic_year DESC";
    $school_years = $DB->get_records_sql($fields . $sql . $where_sql . $order, $params, $page * $per_page, $per_page);

    return array('total_school_years' => $total_school_years, 'school_years' => $school_years);
}

function calculate_days_of_school_year($quarter1, $quarter2, $quarter3, $quarter4, $last_day)
{
    $arr_date = [$quarter1, $quarter2, $quarter3, $quarter4, $last_day];
    $days_of_school_year = 0;
    foreach ($arr_date as $key => $date) {
        $start = new DateTime(date('Y-m-d', $date));

        if (!$arr_date[$key + 1])
            break;

        $end = new DateTime(date('Y-m-d', $arr_date[$key + 1]));
        $interval = $end->diff($start);
        $days = $interval->days;
        $period = new DatePeriod($start, new DateInterval('P1D'), $end);

        foreach ($period as $dt) {
            $curr = $dt->format('D');

            // substract if Saturday or Sunday
            if ($curr == 'Sat' || $curr == 'Sun') {
                $days--;
            }
        }
        $days_of_school_year += $days;
    }
    $holidays = 0;
    $days_of_school_year -= $holidays;

    return $days_of_school_year;
}

function add_school_year($school_year)
{
    global $DB, $CFG;

    if (!isset($school_year->description)) {
        $school_year->description = '';
    }
    if (!isset($school_year->academic_year)) {
        $school_year->academic_year = '';
    }
    if (!isset($school_year->quarter_first)) {
        $school_year->quarter_first = 0;
    }
    if (!isset($school_year->quarter_second)) {
        $school_year->quarter_second = 0;
    }
    if (!isset($school_year->quarter_third)) {
        $school_year->quarter_third = 0;
    }
    if (!isset($school_year->quarter_fourth)) {
        $school_year->quarter_fourth = 0;
    }
    if (!isset($school_year->last_day)) {
        $school_year->last_day = 0;
    }
    $school_year->id = $DB->insert_record('school_year', $school_year);

    return $school_year->id;
}

function update_school_year($school_year)
{
    global $DB;

    $DB->update_record('school_year', $school_year);
}

function delete_school_year($school_year) {
    global $DB;

    $DB->delete_records('school_year', array('id'=>$school_year->id));
}

function show_notifications()
{
    if ($_SESSION["update_successfully"]) {
        // Start div.class card-body
        $alert = html_writer::start_div('alert alert-success', array('role' => 'alert'));
        $alert .= $_SESSION["update_successfully"];
        // End div.class card-body
        $alert .= html_writer::end_div();
        unset ($_SESSION["update_successfully"]);
        echo $alert;
    }

    if ($_SESSION["add_new_successfully"]) {
        // Start div.class card-body
        $alert = html_writer::start_div('alert alert-success', array('role' => 'alert'));
        $alert .= $_SESSION["add_new_successfully"];
        // End div.class card-body
        $alert .= html_writer::end_div();
        unset ($_SESSION["add_new_successfully"]);
        echo $alert;
    }

    if ($_SESSION["delete_successfully"]) {
        // Start div.class card-body
        $alert = html_writer::start_div('alert alert-success', array('role' => 'alert'));
        $alert .= $_SESSION["delete_successfully"];
        // End div.class card-body
        $alert .= html_writer::end_div();
        unset ($_SESSION["delete_successfully"]);
        echo $alert;
    }
}

/**
 * @param array $school_years
 * @param context_system $context
 * @param bootstrap_renderer $OUTPUT
 * @return html_table
 * @throws coding_exception
 * @throws moodle_exception
 */
function generate_table(array $school_years, context_system $context, $OUTPUT)
{
    $data = array();
    $editcolumnisempty = true;
    foreach ($school_years['school_years'] as $school_year) {
        $days = calculate_days_of_school_year($school_year->quarter_first,
            $school_year->quarter_second,
            $school_year->quarter_third,
            $school_year->quarter_fourth,
            $school_year->last_day);
        $line = array();
        $editcolumnisempty = false;
        $line[] = $school_year->name;
        $line[] = $school_year->description;
        $line[] = $days;
        $line[] = $days * 9;

        $buttons = array();
        $school_year_manager = has_capability('local/schoolyear:manage', $context);
        if ($school_year_manager) {
            $buttons[] = html_writer::link(new moodle_url('/local/school_year/edit.php', array('delete' => 1, 'id' => $school_year->id)),
                $OUTPUT->pix_icon('t/delete', get_string('delete')),
                array('title' => get_string('delete')));
            $buttons[] = html_writer::link(new moodle_url('/local/school_year/edit.php', array('id' => $school_year->id)),
                $OUTPUT->pix_icon('t/edit', get_string('edit')),
                array('title' => get_string('edit')));
            $editcolumnisempty = false;
        }
        $line[] = implode(' ', $buttons);
        $data[] = new html_table_row($line);
    }

    $table = new html_table();
    $table->head = array(get_string('name', 'local_school_year'), get_string('description', 'local_school_year'),
        get_string('days', 'local_school_year'), get_string('hours', 'local_school_year'));
    if (!$editcolumnisempty) {
        $table->head[] = get_string('edit');
        $table->colclasses[] = 'centeralign action';
    } else {
        // Remove last column from $data.
        foreach ($data as $row) {
            array_pop($row->cells);
        }
    }

    $table->data = $data;
    return $table;
}

